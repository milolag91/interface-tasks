package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

func main() {

	file := os.Args[1]

	dat, err := ioutil.ReadFile(file)

	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(1)
	}

	fmt.Println(string(dat))

}
